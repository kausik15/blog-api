from rest_framework.permissions import BasePermission,SAFE_METHODS


class IsOwnerOrReadOnly(BasePermission):
    message = 'You Must Be the Owner of the Object'
    # safe_methods = ["PUT", "GET"]
    #
    # def has_permission(self, request, view):
    #     if request.method in self.safe_methods:
    #         return True
    #     return False

    def has_object_permission(self, request, view, obj):
        # if request.method in SAFE_METHODS:
        #     return True
        return obj.user == request.user


