from rest_framework.generics import (ListAPIView,
                                     RetrieveAPIView,
                                     UpdateAPIView,
                                     DestroyAPIView,
                                     CreateAPIView,
                                     RetrieveUpdateAPIView)
from posts.models import Post
from .serializers import (PostDetailSerializer,
                          PostListSerializer)
                          # PostCreateUpdateSerializer)
from django.db.models import Q
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAdminUser, IsAuthenticatedOrReadOnly
from .permissions import IsOwnerOrReadOnly
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.pagination import (LimitOffsetPagination,
                                       PageNumberPagination)

from .pagination import PostLimitOffsetPagination, PostPageNumberPagination


class PostCreateApiView(CreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostListSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class PostDetailApiView(RetrieveAPIView):
    queryset = Post
    serializer_class = PostDetailSerializer
    lookup_field = 'slug'


class PostListApiView(ListAPIView):
    # queryset = Post.objects.all()
    serializer_class = PostListSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['title', 'slug']   # http://127.0.0.1:8000/api/posts/?search=Maddy&ordering=title
    # permission_classes = [IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]
    pagination_class = PostPageNumberPagination  # http://127.0.0.1:8000/api/posts/?limit=1&offset=2
                                              # http://127.0.0.1:8000/api/posts/?limit=1&offset=2&q=post

    def get_queryset(self):
        # queryset_list = Post.objects.filter(user=self.request.user)
        queryset_list = Post.objects.all()
        query = self.request.GET.get("q")
        if query:
            queryset_list = queryset_list.filter(
                Q(title__icontains=query) |
                Q(content__icontains=query) |
                Q(user__first_name__icontains=query) |
                Q(user__last_name__icontains=query)
            ).distinct()
        return queryset_list
    # search_fields = ['title', 'slug']
    # pagination_class = PostPageNumberPagination

    # def get_queryset(self):
    #     queryset_list = Post.objects.filter(user=self.request.user)
    #     query = self.request.GET.get("q")
    #     if query:
    #         queryset_list = queryset_list.filter(
    #                 Q(title__icontains=query) |
    #                 Q(slug__icontains=query) |
    #                 Q(content__icontains=query) |
    #                 Q(user__first_name__icontains=query) |
    #                 Q(user__last_name__icontains=query)
    #             ).distinct()
    #     return queryset_list

    # def perform_search(self, serializer):
    #     serializer.search(user=self.request.user)
    # # http:// 127.0.0.1:8000 / api / posts /?search = post & ordering = -publish
    #
    # def get_queryset(self, *args, **kwargs):
    #     # queryset_list = super(PostListApiView, self).get_queryset(*args, **kwargs)
    #     queryset_list = Post.objects.all()
    #     query = self.request.GET.get("q")
    #     if query:
    #         queryset_list = queryset_list.filter(
    #             Q(user__icontains=self.request.user) |
    #             Q(title__icontains=query) |
    #             Q(slug__icontains=query) |
    #             Q(content__icontains=query) |
    #             Q(user__first_name__icontains=query) |
    #             Q(user__last_name__icontains=query)
    #         ).distinct()
    #     # http://127.0.0.1:8000/api/posts/?q=post
    #     return queryset_list


class PostDeleteApiView(DestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostListSerializer
    lookup_field = 'slug'
    permission_classes = [IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]


class PostUpdateApiView(RetrieveUpdateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostListSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]
    lookup_field = 'slug'

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)


# class PostUpdateApiView(RetrieveUpdateAPIView):
#     queryset = Post.objects.all()
#     serializer_class = PostCreateUpdateSerializer
#     lookup_field = 'slug'
#     permission_classes = [IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]
#
#     def perform_update(self, serializer):
#         serializer.save(user=self.request.user)